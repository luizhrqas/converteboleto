
<!-- Main content -->
<section class="content">
	<div class="row">


		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Adicionar Novo Usuário ao Sistema</h3>
					<a href="<?php echo $this->Html->url(['action' => 'add',  ]); ?>" class="btn btn-lg btn-success pull-right"><i class="fa fa-arrow-left"></i> Voltar</a>
				</div> <!-- .box-header -->

				<div class="box-body">

					<?php echo $this->Form->create("User"); ?>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("name", ['label' => 'Nome', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("username", ['label' => 'Usuário', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("password", ['label' => 'Senha', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("status", ['label' => 'Status', 'class' => 'form-control', 'options' => ['Suspended' => 'Suspendido', 'Active' => 'Ativo'], 'default' => 'Active' ]); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("license_id", ['label' => 'Plano', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->button("Salvar", ['class' => 'btn btn-success btn-block']); ?>
					</div>

					<?php echo $this->Form->end(); ?>

				</div> <!-- .box-body -->
			</div> <!-- .box -->
		</div> <!-- .col -->

	</div> <!-- .row -->
</section> <!-- .content -->