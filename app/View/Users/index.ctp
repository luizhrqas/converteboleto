
<!-- Main content -->
<section class="content">
	<div class="row">


		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Usuários do Sistema</h3>
					<a href="<?php echo $this->Html->url(['action' => 'add',  ]); ?>" class="btn btn-lg btn-success pull-right"><i class="fa fa-plus"></i> Adicionar Novo Usuário ao Sistema</a>
				</div> <!-- .box-header -->

				<div class="box-body">


					<table class="table">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Usuário</th>
								<th>Status</th>
								<th>Plano</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($users as $u) : ?>
							<tr>
								<td><?php echo $u['User']['name']; ?></td>
								<td><?php echo $u['User']['username']; ?></td>
								<td><?php echo $u['User']['status']; ?></td>
								<td><?php echo $u['License']['title']; ?></td>
								<td>
									<a href="<?php echo $this->Html->url(['action' => 'edit', $u['User']['id'] ]); ?>" class="btn btn-success"> <i class="fa fa-pencil"></i> Editar</a>
									<a href="<?php echo $this->Html->url(['action' => 'delete', $u['User']['id'] ]); ?>" class="btn btn-danger"> <i class="fa fa-trash"></i> Excluir</a>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>

				</div> <!-- .box-body -->
			</div> <!-- .box -->
		</div> <!-- .col -->

	</div> <!-- .row -->
</section> <!-- .content -->