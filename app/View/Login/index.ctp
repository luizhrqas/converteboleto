<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Converte Boleto</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<?php echo $this->Html->css(array('bootstrap/css/bootstrap.min'));?>

		<!-- Font Awesome -->
		<?php echo $this->Html->css(array('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'));?>

		<!-- Ionicons -->
		<?php echo $this->Html->css(array('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'));?>

		<!-- Theme style -->
		<?php echo $this->Html->css(array('dist/css/AdminLTE.min'));?>
		<!-- iCheck -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="../../index2.html"><b>Billet</b></a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg"><?php echo $this->Session->flash();?></p>
				<?php echo $this->Form->create('User'); ?>
					<div class="form-group has-feedback">
						<?php 
							echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'E-mail'));
						?>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<?php 
							echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Senha'));
						?>

						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
						</div><!-- /.col -->
					</div>
				<?php //echo $this->Form->end(); ?>
			</div><!-- /.login-box-body -->
		</div><!-- /.login-box -->

		<!-- jQuery 2.1.4 -->
		<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
		<!-- Bootstrap 3.3.5 -->
		<?php echo $this->Html->script('bootstrap/js/bootstrap.min');?>
		<!-- iCheck -->
	</body>
</html>
