<?php
echo $this->element('navegacao');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
        		<?php echo $this->Form->create('License'); ?>
					<div class="col-md-12">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Cadastro de Domínio</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?php 
												$label = "Domínio";
												echo $this->Form->input('domain', array('class' => 'form-control', 'label' => $label, 'div' => false));
											?>
										</div><!-- /.form-group -->
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
					        <div class="box-footer">
					            <?php
					              echo $this->Html->link(
					                  '<i class="fa fa-times"></i> Cancel',
					                  '/licenses',
					                  array('class' => 'btn btn-danger', 'escape' => false)
					              );
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo $this->Form->button(
					                  '<i class="fa fa-check"></i> Avançar',
					                  array('class' => 'btn btn-success')
					              );
					            ?>
					            <!-- <button type="submit" class="btn btn-success pull-right">Avançar</button> -->
					        </div>						
    					</div><!-- /.box -->
					</div>
				</div>
        	<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>

<!-- Bootstrap Color Picker -->
<?php echo $this->Html->css(array('plugins/colorpicker/bootstrap-colorpicker.min'));?>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->

<!-- bootstrap color picker -->
<?php echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min'); ?>

<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>
