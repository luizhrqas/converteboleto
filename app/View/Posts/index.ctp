<?php
  echo $this->element('navegacao');
?>

<!-- Main content -->
<section class="content">

  <?php
    echo $this->element('azulejos_campanhas');
  ?>

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Posts</h3>
          <div class="box-tools">
            <div class="input-group" style="width: 150px;">
              <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tr>
              <th>ID</th>
              <th>User</th>
              <th>Date</th>
              <th>Status</th>
              <th>Reason</th>
              <th></th>
            </tr>
            <tr>
              <td>183</td>
              <td>
                <?php
                  echo $this->Html->link(
                      'John Doe',
                      '',
                      array('escape' => false)
                  );
                ?>
              </td>
              <td>11-7-2014</td>
              <td><span class="label label-success">Approved</span></td>
              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
              <td>
                <?php
                  echo $this->Html->link(
                      'Share',
                      'javascript:;',
                      array(
                        'escape'      => false, 
                        'onClick'     => 'modal(1)', 
                        'class'       => 'btn btn-primary btn-sm', 
                        'data-toggle' => 'modal',
                        'data-target' => '#myModal'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Editar',
                      'edit',
                      array(
                        'escape'      => false,  
                        'class'       => 'btn btn-warning btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Excluir',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-danger btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Duplicar',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-info btn-sm'
                      )
                  );
                ?>
              </td>
            </tr>
            <tr>
              <td>219</td>
              <td>Alexander Pierce</td>
              <td>11-7-2014</td>
              <td><span class="label label-warning">Pending</span></td>
              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
              <td>
                <?php
                  echo $this->Html->link(
                      'Share',
                      'javascript:;',
                      array(
                        'escape'      => false, 
                        'onClick'     => 'modal(1)', 
                        'class'       => 'btn btn-primary btn-sm', 
                        'data-toggle' => 'modal',
                        'data-target' => '#myModal'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Editar',
                      'edit',
                      array(
                        'escape'      => false,  
                        'class'       => 'btn btn-warning btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Excluir',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-danger btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Duplicar',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-info btn-sm'
                      )
                  );
                ?>
              </td>
            </tr>
            <tr>
              <td>657</td>
              <td>Bob Doe</td>
              <td>11-7-2014</td>
              <td><span class="label label-primary">Approved</span></td>
              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
              <td>
                <?php
                  echo $this->Html->link(
                      'Share',
                      'javascript:;',
                      array(
                        'escape'      => false, 
                        'onClick'     => 'modal(1)', 
                        'class'       => 'btn btn-primary btn-sm', 
                        'data-toggle' => 'modal',
                        'data-target' => '#myModal'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Editar',
                      'edit',
                      array(
                        'escape'      => false,  
                        'class'       => 'btn btn-warning btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Excluir',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-danger btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Duplicar',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-info btn-sm'
                      )
                  );
                ?>
              </td>
            </tr>
            <tr>
              <td>175</td>
              <td>Mike Doe</td>
              <td>11-7-2014</td>
              <td><span class="label label-danger">Denied</span></td>
              <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
              <td>
                <?php
                  echo $this->Html->link(
                      'Share',
                      'javascript:;',
                      array(
                        'escape'      => false, 
                        'onClick'     => 'modal(1)', 
                        'class'       => 'btn btn-primary btn-sm', 
                        'data-toggle' => 'modal',
                        'data-target' => '#myModal'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Editar',
                      'edit',
                      array(
                        'escape'      => false,  
                        'class'       => 'btn btn-warning btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Excluir',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-danger btn-sm'
                      )
                  );
                  echo '&nbsp;';
                  echo $this->Html->link(
                      'Duplicar',
                      'javascript:;',
                      array(
                        'escape'      => false,
                        'class'       => 'btn btn-info btn-sm'
                      )
                  );
                ?>
              </td>
            </tr>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <li><a href="#">&laquo;</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">&raquo;</a></li>
          </ul>
        </div>
      </div><!-- /.box -->
    </div>
  </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Preview do Post</h4>
      </div>
      <div class="modal-body">
        <div class="">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class='box-header with-border'>
              <div class='user-block'>
                <?php echo $this->Html->image('dist/img/user1-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle'));?>
                <span class='username'><a href="#">Jonathan Burke Jr.</a></span>
                <span class='description'>Shared publicly - 7:30 PM Today</span>
              </div><!-- /.user-block -->
              <div class='box-tools'>
                <button class='btn btn-box-tool'><i class='fa fa-circle-o'></i></button>
                <button class='btn btn-box-tool'><i class='fa fa-minus'></i></button>
                <button class='btn btn-box-tool'><i class='fa fa-times'></i></button>
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class='box-body'>
              <?php echo $this->Html->image('dist/img/photo2.png', array('alt' => 'Photo', 'class' => 'img-responsive pad'));?>
              <p>I took this photo this morning. What do you guys think?</p>
              <button class='btn btn-default btn-xs'><i class='fa fa-share'></i> Share</button>
              <button class='btn btn-default btn-xs'><i class='fa fa-thumbs-o-up'></i> Like</button>
              <span class='pull-right text-muted'>127 likes - 3 comments</span>
            </div><!-- /.box-body -->
            <div class='box-footer box-comments'>
              <div class='box-comment'>
                <!-- User image -->
                <?php echo $this->Html->image('dist/img/user3-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle img-sm'));?>
                <div class='comment-text'>
                  <span class="username">
                    Maria Gonzales
                    <span class='text-muted pull-right'>8:03 PM Today</span>
                  </span><!-- /.username -->
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                </div><!-- /.comment-text -->
              </div><!-- /.box-comment -->
              <div class='box-comment'>
                <!-- User image -->
                <?php echo $this->Html->image('dist/img/user4-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle img-sm'));?>
                <div class='comment-text'>
                  <span class="username">
                    Luna Stark
                    <span class='text-muted pull-right'>8:03 PM Today</span>
                  </span><!-- /.username -->
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                </div><!-- /.comment-text -->
              </div><!-- /.box-comment -->
            </div><!-- /.box-footer -->
            <div class="box-footer">
              <form action="#" method="post">
                <?php echo $this->Html->image('dist/img/user4-128x128.jpg', array('alt' => 'User Image', 'class' =>  'img-circle img-sm img-responsive'));?>
<!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push">
                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                </div>
              </form>
            </div><!-- /.box-footer -->
          </div><!-- /.box -->
        </div><!-- /.col -->      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->
<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<!-- Sparkline -->
<?php echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js');?>
<!-- jvectormap -->
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>
<!-- SlimScroll 1.3.0 -->
<?php echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');?>
<!-- ChartJS 1.0.1 -->
<?php echo $this->Html->script('plugins/chartjs/Chart.min.js');?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?php echo $this->Html->script('dist/js/pages/dashboard2'); ?>

<!-- AdminLTE for demo purposes -->
<?php echo $this->Html->script('dist/js/demo'); ?>

<script type="text/javascript">
  function modal(id){
    // $(".modal-body").html("Lorem Ipsum");
  }
</script>
