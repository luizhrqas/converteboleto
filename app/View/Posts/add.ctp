<?php
echo $this->element('navegacao');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="row">
        		<?php //echo $this->Form->create('Recipe'); ?>
					<div class="col-md-12">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Configurações Gerais</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
								</div>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?php 
												$label = "Nome da Campanha";
												echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
											?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Descrição para Campanha (Para Smartphone e Tablet) <span class='text-red'>*</span>";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Insira a URL do Vídeo <span class='text-red'>*</span>";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Thumbnail do Vídeo (Imagens em HD > 1200px)";
											echo $this->Form->input('', array('label' => $label, 'div' => false, 'type' => 'file'));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Logo (48x48)";
											echo $this->Form->input('', array('label' => $label, 'div' => false, 'type' => 'file'));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Tipo de CTA (Não é possível alterar Tipo de CTA) <span class='text-red'>*</span>";
											$options = array('ads' => 'Ads', 'adsfull' => 'Ads Full', 'optin' => 'Optin', 'optinfull' => 'Optin Full');
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false, 'options' => $options, 'empty' => 'Selecione'));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Tempo para Exibir CTA (seg) <span class='text-red'>*</span>";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<div class="col-md-12">
						<div class="box box-default collapsed-box">
							<div class="box-header with-border">
								<h3 class="box-title">Customização</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
									<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
								</div>
							</div><!-- /.box-header -->
							<div class="box-body" style="display:none;">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?php 
												$label = "Texto para Headline <span class='text-red'>*</span>";
												echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
											?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Texto para Descrição <span class='text-red'>*</span> (Não usar quebra de linha(Enter))";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Link para CTA (http://www.exemplo.com.br)";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
										<?php 
											$label = "Descrição do Link";
											echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false));
										?>
										</div><!-- /.form-group -->
										<div class="form-group">
											<label>Cor da Headline</label>
											<div class="input-group my-colorpicker2">
												<?php echo $this->Form->input('', array('class' => 'form-control', 'label' => false, 'div' => false));?>
												<div class="input-group-addon">
													<i></i>
												</div>
											</div>
										</div><!-- /.form-group -->
										<div class="form-group">
											<label>Cor da Descrição</label>
											<div class="input-group my-colorpicker2">
												<?php echo $this->Form->input('', array('class' => 'form-control', 'label' => false, 'div' => false));?>
												<div class="input-group-addon">
													<i></i>
												</div>
											</div>
										</div><!-- /.form-group -->
										<div class="form-group">
											<label>Cor do Botão</label>
											<div class="input-group my-colorpicker2">
												<?php echo $this->Form->input('', array('class' => 'form-control', 'label' => false, 'div' => false));?>
												<div class="input-group-addon">
													<i></i>
												</div>
											</div>
										</div><!-- /.form-group -->
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<div class="col-md-12">
						<div class="box box-default collapsed-box">
							<div class="box-header with-border">
								<h3 class="box-title">Controles Avançados</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
									<!-- <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
								</div>
							</div><!-- /.box-header -->
							<div class="box-body" style="display:none;">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?php 
												$label = "Mostrar/Ocultar Botão Fechar";
												$options = array('mostrar' => 'Mostrar', 'ocultar' => 'Ocultar');
												echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false, 'options' => $options, 'empty' => 'Selecione'));
											?>
										</div><!-- /.form-group -->
										<div class="form-group">
											<?php 
												$label = "Pausar Vídeo";
												$options = array('sim' => 'Sim', 'nao' => 'Não');
												echo $this->Form->input('', array('class' => 'form-control', 'label' => $label, 'div' => false, 'options' => $options, 'empty' => 'Selecione'));
											?>
										</div><!-- /.form-group -->
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<div class="col-md-12">
						<div class="box box-default">
					        <div class="box-footer">
					            <?php
					              echo $this->Html->link(
					                  'Cancel',
					                  '/',
					                  array('class' => 'btn btn-warning')
					              );
					              echo $this->Html->link(
					                  'Avançar',
					                  'add',
					                  array('class' => 'btn btn-success pull-right')
					              );
					            ?>
					            <!-- <button type="submit" class="btn btn-success pull-right">Avançar</button> -->
					        </div>						
    					</div><!-- /.box -->
					</div>
				</div>
        	<?php //echo $this->Form->end(); ?>
		</div>
		<div class="col-md-6">
			<div class="box box-widget">
				<div class='box-header with-border'>
					<div class='user-block'>
						<?php echo $this->Html->image('dist/img/user1-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle'));?>
						<span class='username'><a href="#">Jonathan Burke Jr.</a></span>
						<span class='description'>Shared publicly - 7:30 PM Today</span>
					</div><!-- /.user-block -->
					<div class='box-tools'>
						<button class='btn btn-box-tool'><i class='fa fa-circle-o'></i></button>
						<button class='btn btn-box-tool'><i class='fa fa-minus'></i></button>
						<button class='btn btn-box-tool'><i class='fa fa-times'></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class='box-body'>
					<?php 
						echo $this->Html->image('dist/img/photo2.png', array('alt' => 'Photo', 'class' => 'img-responsive pad'));
					?>
					<p>I took this photo this morning. What do you guys think?</p>
					<button class='btn btn-default btn-xs'><i class='fa fa-share'></i> Share</button>
					<button class='btn btn-default btn-xs'><i class='fa fa-thumbs-o-up'></i> Like</button>
					<span class='pull-right text-muted'>127 likes - 3 comments</span>
				</div><!-- /.box-body -->
				<div class='box-footer box-comments'>
					<div class='box-comment'>
						<!-- User image -->
						<?php 
							echo $this->Html->image('dist/img/user3-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle img-sm'));
						?>
						<div class='comment-text'>
							<span class="username">
								Maria Gonzales
								<span class='text-muted pull-right'>8:03 PM Today</span>
							</span><!-- /.username -->
							It is a long established fact that a reader will be distracted
							by the readable content of a page when looking at its layout.
						</div><!-- /.comment-text -->
					</div><!-- /.box-comment -->
					<div class='box-comment'>
						<!-- User image -->
						<?php 
							echo $this->Html->image('dist/img/user4-128x128.jpg', array('alt' => 'User Image', 'class' => 'img-circle img-sm'));
						?>
						<div class='comment-text'>
							<span class="username">
								Luna Stark
								<span class='text-muted pull-right'>8:03 PM Today</span>
							</span><!-- /.username -->
							It is a long established fact that a reader will be distracted
							by the readable content of a page when looking at its layout.
						</div><!-- /.comment-text -->
					</div><!-- /.box-comment -->
				</div><!-- /.box-footer -->
				<div class="box-footer">
					<form action="#" method="post">
						<?php 
							echo $this->Html->image('dist/img/user4-128x128.jpg', array('alt' => 'User Image', 'class' =>  'img-circle img-sm img-responsive'));
						?>
						<!-- .img-push is used to add margin to elements next to floating images -->
						<div class="img-push">
							<input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
						</div>
					</form>
				</div><!-- /.box-footer -->
			</div><!-- /.box -->
		</div>
	</div>
</section>

<!-- Bootstrap Color Picker -->
<?php echo $this->Html->css(array('plugins/colorpicker/bootstrap-colorpicker.min'));?>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->

<!-- bootstrap color picker -->
<?php echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min'); ?>

<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<script>
    //color picker with addon
    $(".my-colorpicker2").colorpicker();
</script>
