<?php echo $this->Html->css(array('plugins/datatables/dataTables.bootstrap.css'));?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $product[ 'Product' ][ 'product_name' ];?>
   	<small><?php echo Inflector::humanize( $type );?></small>
   	<?php
		echo $this->Html->link(
			'<i class="fa fa-pencil"></i> Adicionar',
			array( 'action' => 'add', $type, $product_code),
			array(
				'escape'      => false,  
				'class'       => 'btn btn-success btn-xs'
			)
		);
    ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mensagens</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">

			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Mensagens Cadastradas</a></li>
					<li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Mensagens Agendadas</a></li>
					<li><a href="#tab_3" data-toggle="tab">Mensagens Enviadas</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<?php echo $this->element( 'messages-list' );?>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_2">
						<?php echo $this->element( 'messages-list-schedule' );?>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_3">
						<?php echo $this->element( 'messages-list-sent' );?>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
		</div>
	</div>
</section>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>

<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>

<?php echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('plugins/datatables/dataTables.bootstrap.min.js'); ?>

<?php echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');?>

<?php echo $this->Html->script('plugins/fastclick/fastclick.min.js');?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<!-- AdminLTE for demo purposes -->
<?php echo $this->Html->script('dist/js/demo'); ?>

    <script>
    	// var $ = jQuery.noConflict();
      	$(function () {
			$('#messages, #schedules, #sents').DataTable({
				"paging": true,
				      "lengthChange": false,
				      "searching": true,
				      "ordering": true,
				      "info": true,
				      "autoWidth": false,
				      "language": {
						    "sEmptyTable": "Nenhum registro encontrado",
						    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
						    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
						    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
						    "sInfoPostFix": "",
						    "sInfoThousands": ".",
						    "sLengthMenu": "_MENU_ resultados por página",
						    "sLoadingRecords": "Carregando...",
						    "sProcessing": "Processando...",
						    "sZeroRecords": "Nenhum registro encontrado",
						    "sSearch": "Pesquisar",
						    "oPaginate": {
						        "sNext": "Próximo",
						        "sPrevious": "Anterior",
						        "sFirst": "Primeiro",
						        "sLast": "Último"
						    },
						    "oAria": {
						        "sSortAscending": ": Ordenar colunas de forma ascendente",
						        "sSortDescending": ": Ordenar colunas de forma descendente"
						    }
						}			
			});
     	});
    </script>
