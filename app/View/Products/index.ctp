<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Produtos</h3>
					<div class="box-tools" style="display:none;">
						<div class="input-group" style="width: 150px;">
							<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">
                  	<table id="example2" class="table table-bordered table-hover">
                  		<thead>
						<tr>
							<th>#</th>
							<th>Produto</th>
							<th>Ações</th>
						</tr>
						</thead>
						<tbody>
							<?php
								foreach ($products as $p) :
							?>
								<tr>
									<td><?php echo $p['Product']['product_id']; ?></td>
									<td>
										<?php 
											echo '<p class="lead" style="margin-bottom:0">' . $p['Product']['product_name'] . '</p>';

											echo $this->Html->link(
												'<i class="fa fa-pencil-square-o"></i> Editar',
												array( 'action' => 'edit', $p['Product']['product_code'] ),
												array(
													'escape'      => false,  
													'class'       => 'btn btn-warning btn-xs'
												)
											);
											echo '&nbsp;';
											echo '&nbsp;';

											echo $this->Form->postLink('<i class="fa fa-times"></i> Excluir', array('action' => 'delete', $p['Product']['id']), array('escape' => false, 'class' => 'btn btn-danger btn-xs'), __('Tem certeza de que deseja excluir # %s?', $p['Product']['product_id']));

										?>
									</td>
									<td>
										<?php
											echo $this->Html->link(
												'<i class="fa fa-dashboard"></i> Dashboard',
												array( 'controller' => 'products', 'action' => 'dashboard', $p['Product']['product_code'] ),
												array(
													'escape'      => false,  
													'class'       => 'btn btn-primary btn-lg'
												)
											);
											echo '&nbsp;';
											echo '&nbsp;';

											echo $this->Html->link(
												'<i class="fa fa-mobile"></i> SMS',
												array( 'controller' => 'messages', 'action' => 'index', 'sms', $p['Product']['product_code'] ),
												array(
													'escape'      => false,  
													'class'       => 'btn btn-lg bg-teal-active color-palette'
												)
											);
											echo '&nbsp;';
											echo '&nbsp;';

											echo $this->Html->link(
												'<i class="fa fa-whatsapp"></i> Whatsapp',
												array( 'controller' => 'messages', 'action' => 'index', 'whatsapp', $p['Product']['product_code'] ),
												array(
													'escape'      => false,  
													'class'       => 'btn btn-success btn-lg'
												)
											);
											echo '&nbsp;';
											echo '&nbsp;';
										?>
									</td>
								</tr>
							<?php
								endforeach;
							?>
						</body>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>
