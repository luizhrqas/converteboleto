<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $product[ 'Product' ][ 'product_name' ];?> <small>Dashboard</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Produtos</a></li>
    <li><a href="#"><?php echo $product[ 'Product' ][ 'product_name' ];?></a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  
<!-- Info boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-file-pdf-o"></i></span>
        <div class="info-box-content">
          <?php
              echo $this->Html->link(
                  '
                  <span class="info-box-text">Boletos</span>
                  <span class="info-box-number">' . $billets . '</span>
                  <span class="info-box-text">Gerados</span>
                  ',
                  '#',
                  array('escape' => false)
              );
          ?>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-teal-active color-palette"><i class="fa fa-mobile"></i></span>
        <div class="info-box-content">
          <?php
              echo $this->Html->link(
                  '
                  <span class="info-box-text">SMS</span>
                  <span class="info-box-number">' . $message[0][0][ 'qty' ] . '</span>
                  <span class="info-box-text">Mensagens</span>
                  ',
                  array( 'controller' => 'messages', 'action' => 'index', 'sms', $product_code ),
                  array('escape' => false)
              );
          ?>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-whatsapp"></i></span>
        <div class="info-box-content">
          <?php
              echo $this->Html->link(
                  '
                  <span class="info-box-text">Whatsapp</span>
                  <span class="info-box-number">' . $message[1][0][ 'qty' ] . '</span>
                  <span class="info-box-text">Mensagens</span>
                  ',
                  array( 'controller' => 'messages', 'action' => 'index', 'whatsapp', $product_code ),
                  array('escape' => false)
              );
          ?>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-exchange"></i></span>
        <div class="info-box-content">
          <?php
              echo $this->Html->link(
                  '
                  <span class="info-box-text">Sync</span>
                  <span class="info-box-number">' . $syncs . '</span>
                  <span class="info-box-text">E-mails</span>
                  ',
                  '#',
                  array('escape' => false)
              );
          ?>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Boletos Gerados X Compensados</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <div class="btn-group">
              <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><?php echo $this->Html->link( 'Últimos 7 dias', array( 'action' => 'dashboard', $product_code, 7 ), array('escape' => false) )?></li>
                <li><?php echo $this->Html->link( 'Últimos 15 dias', array( 'action' => 'dashboard', $product_code, 15 ), array('escape' => false) )?></li>
                <li><?php echo $this->Html->link( 'Últimos 30 dias', array( 'action' => 'dashboard', $product_code, 30 ), array('escape' => false) )?></li>
              </ul>
            </div>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <p class="text-center">
                <strong><?php echo date( 'd/m/Y', strtotime( '-' . $period . ' days' ) );?> - <?php echo date('d/m/Y');?></strong>
              </p>
              <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 280px;"></canvas>
              </div><!-- /.chart-responsive -->
            </div><!-- /.col -->
            <div class="col-md-12">
              <div id="js-legend" class="chart-legend"></div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- ./box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Últimos Boletos</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Status</th>
                  <th>Data</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ( $billetsList as $b ) {

                  $class_default = 'label-warning';
                  $label_default = 'Gerado';
                  if ( $b[ 'Billet' ][ 'status' ] == 'Closed' ) {
                    $class_default = 'label-success';
                    $label_default = 'Compensado';
                  }
                ?>
                <tr>
                  <td><?php echo $b[ 'Transaction' ][ 'Client' ][ 'name' ];?></td>
                  <td><span class="label <?php echo $class_default;?>"><?php echo $label_default;?></span></td>
                  <td><?php echo date( 'd/m/Y H:i:s', strtotime( $b[ 'Transaction' ][ 'purchase_date' ] ) )?></td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
          <br />
        </div><!-- /.box-footer -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->

<style type="text/css">
.chart-legend ul li {list-style: none}
</style>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<!-- SlimScroll 1.3.0 -->
<?php echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');?>
<!-- ChartJS 1.0.1 -->
<?php echo $this->Html->script('plugins/chartjs/Chart.min.js');?>

<script type="text/javascript">

jQuery(function () {

  'use strict';

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = jQuery("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: [<?php echo $dates;?>],
    datasets: [
      {
        label: "Boletos Gerados",
        fillColor: "rgba(220,220,220,0.2)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(220,220,220,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(220,220,220,1)",
        data: [<?php echo $billets_generates;?>]
      },
      {
        label: "Boletos Compensados",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
        data: [<?php echo $billets_pay;?>]
      }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: false,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><div style=\"background-color:<%=datasets[i].strokeColor%>; width: 20px; height: 20px; float: left\"></div>&nbsp;<%=datasets[i].label%><br/></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  var myLineChart = salesChart.Line(salesChartData, salesChartOptions);
  document.getElementById('js-legend').innerHTML = myLineChart.generateLegend();

  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------
});
</script>