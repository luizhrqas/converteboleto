<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Produtos
   	<small>Adicionar Produto</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#"> Produtos</a></li>
    <li class="active">Adicionar Produto</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
        		<?php echo $this->Form->create('Product'); ?>
					<div class="col-md-12">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Adicionar Produto</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<?php 
												echo $this->Form->input('product_id', array('class' => 'form-control', 'label' => 'Código do Produto', 'div' => false, 'type' => 'text'));
											?>
										</div><!-- /.form-group -->

										<div class="form-group">
											<?php 
												echo $this->Form->input('product_name', array('class' => 'form-control', 'label' => 'Nome do Produto', 'div' => false));
											?>
										</div><!-- /.form-group -->

										<div class="form-group">
											<?php 
												echo $this->Form->input('Configuration.autoresponder_token', array('class' => 'form-control', 'label' => 'Autoresponder Token', 'div' => false));
											?>
										</div><!-- /.form-group -->

										<div class="form-group">
											<?php 
												echo $this->Form->input('Configuration.autoresponder_url', array('class' => 'form-control', 'label' => 'Autoresponder URL', 'div' => false));
											?>
										</div><!-- /.form-group -->

										<div class="form-group">
											<?php 
												echo $this->Form->input('Configuration.field_name', array('class' => 'form-control', 'label' => 'Nome do Campo', 'div' => false));
											?>
										</div><!-- /.form-group -->

									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
					        <div class="box-footer">
					            <?php
					              echo $this->Html->link(
					                  '<i class="fa fa-times"></i> Cancel',
					                  array( 'action' => 'index' ),
					                  array('class' => 'btn btn-danger', 'escape' => false)
					              );
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo $this->Form->button(
					                  '<i class="fa fa-check"></i> Avançar',
					                  array('class' => 'btn btn-success')
					              );
					            ?>
					            <!-- <button type="submit" class="btn btn-success pull-right">Avançar</button> -->
					        </div>						
    					</div><!-- /.box -->
					</div>
				</div>
        	<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>

<!-- Bootstrap Color Picker -->
<?php echo $this->Html->css(array('plugins/colorpicker/bootstrap-colorpicker.min'));?>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->

<!-- bootstrap color picker -->
<?php echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min'); ?>

<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>
