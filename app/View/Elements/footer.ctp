      <footer class="main-footer">
        <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="https://bendit.com.br" target="_blank">Bendit</a>.</strong> All rights reserved.

		<?php echo $this->element('sql_dump');?>
      </footer>
