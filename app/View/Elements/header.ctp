<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo $this->Html->url('/'); ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>CB</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Converte</b> Boleto</span>
  </a>

</header>