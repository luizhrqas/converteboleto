      	<table id="schedules" class="table table-bordered table-hover">
      		<thead>
			<tr>
				<th>Cliente</th>
				<th>Telefone</th>
				<th>Título</th>
				<th>Data de Envio</th>
				<th>Tipo</th>
			</tr>
			</thead>
			<tbody>
				<?php
					foreach ($scheduledMessages as $m) :
				?>
					<tr>
						<td><?php echo $m['Client']['name']; ?></td>
						<td><?php echo $m['Client']['phone_local_code'] . $m['Client']['phone_number']; ?></td>
						<td>
							<?php 
								echo $m['Message']['title'];
							?>
						</td>
						<td><?php echo date( 'd/m/Y H:i:s', strtotime( $m['ScheduledMessage']['created'] ) ) ?></td>
						<td>
							<?php
								echo Inflector::humanize( $type );
							?>
						</td>
					</tr>
				<?php
					endforeach;
				?>
			</body>
		</table>