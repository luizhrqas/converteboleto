      	<table id="messages" class="table table-bordered table-hover">
      		<thead>
			<tr>
				<th>#</th>
				<th>Título</th>
				<th>Aguardar</th>
				<th>Tipo</th>
			</tr>
			</thead>
			<tbody>
				<?php
					foreach ($messages as $m) :
				?>
					<tr>
						<td><?php echo $m['Message']['id']; ?></td>
						<td>
							<?php 
								echo $m['Message']['title'];
								echo '<br />';

								echo $this->Html->link(
									'<i class="fa fa-pencil-square-o"></i> Editar',
									'edit/' . $type . '/' . $product_code . '/' . $m['Message']['message_code'],
									array(
										'escape'      => false,  
										'class'       => 'btn btn-warning btn-xs'
									)
								);
								echo '&nbsp;';
								echo '&nbsp;';

								echo $this->Form->postLink('<i class="fa fa-times"></i> Excluir', array('action' => 'delete', $type, $product_code, $m['Message']['message_code'] ), array('escape' => false, 'class' => 'btn btn-danger btn-xs'), __('Tem certeza de que deseja excluir # %s?', $m['Message']['id']));
							?>
						</td>
						<td><?php echo $m['Message']['send_at']; ?> dias</td>
						<td>
							<?php
								echo Inflector::humanize( $type );
							?>
						</td>
					</tr>
				<?php
					endforeach;
				?>
			</body>
		</table>