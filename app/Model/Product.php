<?php
App::uses('AppModel', 'Model');

class Product extends AppModel {
	
	public function beforeFind($query = array()) {
		$query[ 'conditions' ][] = array( 'Product.user_id' => AuthComponent::user('id') );
		return $query;
	}

	public function beforeSave($options = array()) {
		$this->data['Product']['user_id'] = AuthComponent::user('id');
		$this->data['Product']['product_code'] = md5( $this->data['Product']['product_id'] );
		return true;
	}

	public $validate = array(
		'domain' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasOne = array(
		'Configuration' => array(
			'className' => 'Configuration',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
