<?php
App::uses('AppModel', 'Model');

class Message extends AppModel {

	public function afterSave( $created, $options = array() ) {
		if ( $created ) {
			$this->data['Message']['message_code'] = md5( $this->id . $this->data['Message'][ 'title' ] );
			$this->save( $this->data );
		}
		
		return true;
	}

	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
