<?php
App::uses('AppModel', 'Model');
/**
 * Arquivo Model
 *
 * @property Aula $Aula
 */
class License extends AppModel {

	public $hasMany = array("User");

	public $validate = array(
		'title' => array(
			'notEmpty',
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Este plano já existe. Utilize outro.'
			)
		),
		'product_limit' => array(
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Mínimo de 1 caracteres.',
			)
		),
		'message_Limit' => array(
			'minLength' => array(
				'rule' => array('minLength', 1),
				'message' => 'Mínimo de 1 caracteres.',
			)
		)
	);
}
