<?php
App::uses('AppController', 'Controller');

class DataMinnerController extends AppController {

public $uses = array( 'Affiliate', 'Billet', 'Client', 'Producer', 'Redirect', 'Subscription', 'Sync', 'Transaction' );
private $dataPost;

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index');

		$this->autoRender = false;
		$this->layout = false;
	}

	public function index() {
		$this->hotmart();
	}

	private function hotmart(){
		$this->loadModel( 'HotmartPost' );

		$options = array( 
			'conditions' => array( 'HotmartPost.status' => 'New' ),
			'order' => array( 'HotmartPost.id ASC' ),
			'count' => 2
		);
		
		$posts = $this->HotmartPost->find( 'all', $options );

		foreach ( $posts as $p ) {

			// pr( '=======================' );
			// pr( json_decode( $p[ 'HotmartPost' ][ 'post' ] ) );

			// 0. Set Properties
			$this->dataPost = json_decode( $p[ 'HotmartPost' ][ 'post' ] );
			$this->dataPost->hotmart_post_id = $p[ 'HotmartPost' ][ 'id' ];

			// 1. Producer
			$this->dataPost->producer_id = $this->setInit( 'Producer', 'producer_document' );

			// 2. Client
			$this->dataPost->client_id = $this->setInit( 'Client', 'doc' );

			// 3. Affiliate
			$this->dataPost->affiliate_id = $this->setInit( 'Affiliate', 'aff' );

			// 4. Transaction
			$this->dataPost->transaction_id = $this->transactions();

			// 5. Subscription
			$this->dataPost->subscription_id = $this->subscriptions();

			// 6. Billet
			$this->dataPost->billet_id = $this->billets();

			// 6.1 Redirect
			$this->dataPost->redirect_id = $this->redirects();

			// 7. Sync
			$this->dataPost->sync_id = $this->sync();

			// 8. Update status of Hotmart Post
			$this->HotmartPost->read( null, $p[ 'HotmartPost' ][ 'id' ] );
			$this->HotmartPost->set( 'status', 'Proccessed' );
			$this->HotmartPost->save();

			// pr( '=======================' );
			// pr( 'Hotmart Post ID: ' . $p[ 'HotmartPost' ][ 'id' ] . ' - Transaction ID: ' . $this->dataPost->transaction_id );
			// pr( '=======================' );

			// 9. Clean Properties
			$this->dataPost = '';
		}
	}

	private function setInit( $model, $key = '' ) {
		
		$options = array(
			'conditions' => array( $model . '.' . $key => $this->dataPost->{$key} )
		);

		$reg = $this->{$model}->find( 'first', $options );

		if ( !$reg ) {

			$columns = $this->{$model}->schema();

			foreach ( $columns as $i => $v ) {

				if ( $i != 'id' && $i != 'created' && $i != 'modified' )
					$this->request->data[ $model ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
			}

			$this->{$model}->create();
			$this->{$model}->save( $this->request->data );

			$id = $this->{$model}->id;
		} else {
			$id = $reg[ $model ][ 'id' ];
		}

		return $id;
	}

	private function transactions() {

		$columns = $this->Transaction->schema();

		foreach ( $columns as $i => $v ) {

			if ( $i != 'id' && $i != 'created' && $i != 'modified' )
				$this->request->data[ 'Transaction' ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
		}

		if ( !isset( $this->request->data[ 'Transaction' ][ 'src' ] ) || empty( $this->request->data[ 'Transaction' ][ 'src' ] ) )
			$this->request->data[ 'Transaction' ][ 'src' ] = $this->dataPost->srk;

		$this->Transaction->create();
		$this->Transaction->save( $this->request->data );

		return $this->Transaction->id;
	}

	private function subscriptions() {

		$columns = $this->Subscription->schema();

		foreach ( $columns as $i => $v ) {

			if ( $i != 'id' && $i != 'created' && $i != 'modified' )
				$this->request->data[ 'Subscription' ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
		}

		if( $this->request->data[ 'Subscription' ][ 'name_subscription_plan' ] ) {
			$this->Subscription->create();
			$this->Subscription->save( $this->request->data );
		}

		return $this->Subscription->id;
	}

	private function billets() {

		$columns = $this->Billet->schema();

		foreach ( $columns as $i => $v ) {

			if ( $i != 'id' && $i != 'created' && $i != 'modified' )
				$this->request->data[ 'Billet' ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
		}

		if( isset( $this->dataPost->billet_url ) && !empty( $this->dataPost->billet_url ) && $this->dataPost->status == 'billet_printed' ) {
			$this->Billet->create();
			$this->Billet->save( $this->request->data );
		}

		return $this->Billet->id;
	}

	private function redirects() {

		$columns = $this->Redirect->schema();

		foreach ( $columns as $i => $v ) {

			if ( $i != 'id' && $i != 'created' && $i != 'modified' )
				$this->request->data[ 'Redirect' ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
		}

		if( isset( $this->dataPost->billet_url ) && !empty( $this->dataPost->billet_url ) && $this->dataPost->status == 'billet_printed' ) {

			$this->request->data['Redirect']['code'] = '-';
			$this->Redirect->create();
			$this->Redirect->save($this->request->data);
			$this->request->data['Redirect']['code'] = md5($this->Redirect->id);
			$this->Redirect->save($this->request->data);
		}

		return $this->Redirect->id;
	}

	private function sync() {

		$columns = $this->Sync->schema();

		foreach ( $columns as $i => $v ) {

			if ( $i != 'id' && $i != 'created' && $i != 'modified' )
				$this->request->data[ 'Sync' ][ $i ] 			= ( isset( $this->dataPost->{$i} ) && !empty( $this->dataPost->{$i} )  ) ? $this->dataPost->{$i} : '';
		}

		$this->request->data[ 'Sync' ][ 'status' ] = 'New';

		if( isset( $this->dataPost->billet_url ) && !empty( $this->dataPost->billet_url ) && $this->dataPost->status == 'billet_printed' ) {

			$this->request->data['Sync']['product_id'] = $this->dataPost->prod;
			$this->Sync->create();
			$this->Sync->save($this->request->data);
		}

		return $this->Sync->id;
	}
}