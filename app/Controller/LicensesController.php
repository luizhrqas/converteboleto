	<?php

App::uses('AppController', 'Controller');

class LicensesController extends AppController {

	public $components = array('Paginator');

	public function index(){

		// Busca todas as licenças do usuário
		$licenses = $this->License->find('all', array(
			'contain' => array('User')
		));

		// Envia dados para view
		$this->set('titulo', 'Licenças');
		$this->set('licenses', $licenses);
		$this->set('menu', 'licenses');
	}

	public function edit($id = null){
		$this->set('titulo', 'Edição de Plano');
		$this->set('menu', 'license');

		if (!$this->License->exists($id)) {
			throw new NotFoundException(__('Esse plano não existe!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->License->save($this->request->data)) {
				$this->Session->setFlash(__('Plano salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O plano não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array('conditions' => array('License.' . $this->License->primaryKey => $id));
			$this->request->data = $this->License->find('first', $options);
		}
	}

}
