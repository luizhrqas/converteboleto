<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	public $components = array('Qimage');

/**
 * Lista todos os usuários participantes.
 */
	public function index() {
		$options = array(
			'contain' => array('Licenses')
		);
		$users = $this->User->find("all", $options);

		// Envia dados para view
		$this->set(compact("users"));
		$this->set("titulo", "Usuários do Sistema");
		$this->set("menu", "users");
	}

/**
 * Adiciona novo usuário.
 */
	public function add($id = null)
	{

		$licenses = $this->User->License->find("list");

		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possível salvar o usuário.'), 'default', array('class' => 'callout callout-danger'));
			}

		}

		// Envia dados para view
		$this->set(compact("users", "licenses"));
		$this->set("titulo", "Usuários do Sistema");
		$this->set("menu", "users");
	}

/**
 * Edita o usuário selecionado.
 */
	public function edit($id = null)
	{

		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possível salvar o usuário.'), 'default', array('class' => 'callout callout-danger'));
			}

		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);

			unset($this->request->data['User']['password']);
		}
		
		$licenses = $this->User->License->find("list");

		// Envia dados para view
		$this->set(compact("users", "licenses"));
		$this->set("titulo", "Usuários do Sistema");
		$this->set("menu", "users");
	}

/**
 * Remove o usuário selecionado.
 */
	public function delete($id = null)
	{
		$this->User->id = $id;

		if($this->User->exists()) {
			$this->User->delete();
			$this->Session->setFlash("Usuário removido.");
		}

		return $this->redirect('/users/index');
	}

	public function profile(){
		$this->set('titulo', 'Perfil');
		$this->set('menu', 'profile');

		$id = AuthComponent::user('id');

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuário não existe!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			// Upload
			if(!empty($this->data['User']['photo']['name'])){
				$this->request->data['User']['photo'] = $this->Qimage->copy(array('file' => $this->data['User']['photo'], 'path' => 'files/'));
			}else{
				if(!empty($this->data['User']['photo_h'])){
				 $this->request->data['User']['photo'] = $this->data['User']['photo_h'];
				}
			}
			// Upload

			if ($this->User->save($this->request->data)) {

				$this->Session->write('Auth.User.name', $this->request->data['User']['name']);
				$this->Session->write('Auth.User.username', $this->request->data['User']['username']);
				$this->Session->write('Auth.User.photo', $this->request->data['User']['photo']);

				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'profile'));
			} else {
				$this->Session->setFlash(__('Não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}

	}
}
