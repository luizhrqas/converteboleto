<?php
App::uses('AppController', 'Controller');

class ProductsController extends AppController {

	var $uses = array( 'Billet', 'Message', 'Product', 'Transaction', 'Sync', 'License' );

	public function beforeFilter() {
		parent::beforeFilter();

		$this->set( 'menu', 'products' );
	}

	// Lista de Produtos
	public function index() {

		$license = $this->License->find("first", array(
			'conditions' => array(
					'License.id' => AuthComponent::user('license_id')
			)
		) );
		$products = $this->Product->find( 'all' );

		if(count($products) >= $license['License']['product_limit']) {
			$limite = $license['License']['product_limit'];
			$total = count($products);
			$this->Session->setFlash("Você já estourou o limite de produtos que você pode cadastrar, de acordo com seu plano. Seu plano permite $limite produtos. Você tem $total já cadastrado.", 'default', array('class' => 'callout callout-warning'));
		}
		$this->set( 'products', $products );
		$this->set(compact("license"));
	}

	// Dashboard por produto
	public function dashboard( $product_code = null, $period = 7 ) {
		$product = $this->Product->find( 'first', array( 'conditions' => array( 'Product.product_code' => $product_code ) ) );
		$this->set( 'product', $product );	
		$this->set( 'product_code', $product_code );
		$this->set( 'billets', $this->Billet->find( 'count', array( 'conditions' => array( 'Billet.transaction_id' => $this->getTransactionIds( $this->Transaction->find( 'all', array( 'conditions' => array(  'Transaction.prod' => $product[ 'Product' ][ 'product_id' ] ) ) ) ) ) ) ) );
		$this->set( 'message', $this->Message->find( 'all', array( 'conditions' => array( 'Message.product_id' => $product[ 'Product' ][ 'id' ] ), 'group' => array( 'Message.type' ), 'fields' => array( 'Message.type', 'count(Message.id) AS qty ' ) ) ) );
		$this->set( 'syncs', $this->Sync->find( 'count', array( 'conditions' => array( 'Sync.product_id' => $product[ 'Product' ][ 'product_id' ], 'Sync.status' => 'Proccessed' ) ) ) );
		$this->set( 'billetsList', $this->Billet->find( 'all', array( 'limit' => 7, 'order' => array( 'Billet.created DESC' ) , 'conditions' => array( 'Billet.transaction_id' => $this->getTransactionIds( $this->Transaction->find( 'all', array( 'conditions' => array(  'Transaction.prod' => $product[ 'Product' ][ 'product_id' ] ) ) ) ) ) ) ) );
		$this->set( 'period', $period );

		$dates  = $this->getDates( $period );
		$this->set( 'dates', $this->ptBrDates( $dates ) );

		$qtyPeriodGenerate = $this->Billet->query( 'SELECT created, count(id) AS qty FROM  billets WHERE DATE(created) IN ( ' . $dates . ' ) AND status = "Open" GROUP BY DATE(created)' );
		$this->set( 'qtyPeriodGenerate', $qtyPeriodGenerate );
		$this->set( 'billets_generates', $this->formatDataForGraph( $dates, $qtyPeriodGenerate ) );

		$qtyPeriodPay = $this->Billet->query( 'SELECT created, count(id) AS qty FROM  billets WHERE DATE(created) IN ( ' . $dates . ' ) AND status = "Closed" GROUP BY DATE(created)' );
		$this->set( 'qtyPerioPay', $qtyPeriodPay );
		$this->set( 'billets_pay', $this->formatDataForGraph( $dates, $qtyPeriodPay ) );
	}

	public function add(){
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		}

		if ( count( $this->Product->find( 'all' ) ) >= AuthComponent::user('License.product_limit') ) {
			$this->Session->setFlash(__('Você adicionou todos os produtos'), 'default', array('class' => 'callout callout-danger'));
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function edit( $product_code = null ){
		$product = $this->Product->find( 'first', array( 'conditions' => array( 'Product.product_code' => $product_code ) ) );
		$id = $product['Product']['id'];
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Esse Produto não existe!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
	}

	public function delete($id = null){
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Esse Produto não existe!'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('Produto excluído'), 'default', array('class' => 'callout callout-success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Produto não foi excluído'), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index'));
	}

	private function getTransactionIds( $transactions ) {
		$ids = array();
		foreach ( $transactions as $t ) {
			$ids[] = $t[ 'Transaction' ][ 'id' ];
		}

		return $ids ;
	}

	private function ptBrDates( $dates ) {

		$dates = explode( ',', str_replace( '"', '', $dates ) );

		$arr = array();
		foreach ( $dates as $d ) {
			$arr[] = '"' . date( 'd/m/Y', strtotime( $d ) ) . '"';
		}

		return implode( ',', $arr );
	}

	private function getDates( $p = 7 ) {

		$regs = '';

		for ( $i = $p; $i >= 0; $i-- ) { 
			
			if ( !empty( $regs ) ) {
				$regs .= ',' . '"' . date( 'Y-m-d', strtotime( '-' . $i . ' days' ) ) . '"';
			} else {
				$regs .= '"' . date( 'Y-m-d', strtotime( '-' . $i . ' days' ) ) . '"';
			}
		}

		return $regs;
	}

	private function formatDataForGraph( $dates, $qty ) {

		// Sample: [65, 70, 80, 85, 90, 105, 110]

		$arr = explode( ',', str_replace( '"', '', $dates ) );
		$regs = array();

		// Trata dados
		for ( $i = 0; $i < count($qty); $i++ ) {
			$qty2[$i][ 'created' ] = date('Y-m-d', strtotime($qty[$i][ 'billets' ][ 'created' ]));
		}

		for ( $c = 0; $c < count($arr); $c++ ) {

			$r = array_search( $arr[$c], array_column( $qty2, 'created') );

			if ( $r !== false ) {
				$regs[] = $qty[$r][0]['qty'];
			} else {
				$regs[] = 0;
			}
		}

		return implode( ',', $regs );
	}	
}
