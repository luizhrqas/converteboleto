<?php
App::uses('AppController', 'Controller');

class MessagesController extends AppController {

	var $uses = array( 'Message', 'MessagesSent', 'ScheduledMessage' );

	public function beforeFilter() {
		parent::beforeFilter();

		$this->set( 'menu', 'products' );
	}

	public function index( $type = null, $product_code = null ) {
		$this->set( 'type', $type );
		$this->set( 'product_code', $product_code );
		$product = $this->productDetails( $product_code );
		$this->set( 'product', $product );
		$messages = $this->Message->find( 'all', array( 'conditions' => array( 'Message.product_id' => $product[ 'Product' ][ 'id' ], 'Message.type' => $type ) ) );
		$this->set( 'messages', $messages );
		$ids = $this->getMessageIds( $messages );
		$this->set( 'scheduledMessages', $this->ScheduledMessage->find( 'all', array( 'conditions' => array( 'ScheduledMessage.message_id' => $ids ) ) ) );
		$this->set( 'messagesSent', $this->MessagesSent->find( 'all', array( 'conditions' => array( 'MessagesSent.message_id' => $ids ) ) ) );
	}

	public function add( $type = null, $product_code = null ) {
		if ($this->request->is('post')) {
			$this->Message->create();
			if ($this->Message->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index', $this->request->data[ 'Message' ][ 'type' ], $this->request->data[ 'Message' ][ 'product_code' ]));
			} else {
				$this->Session->setFlash(__('Não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		}

		$this->set( 'type', $type );
		$this->set( 'product_code', $product_code );
		$this->set( 'product', $this->productDetails( $product_code ) );

		// if ( count( $this->Message->find( 'all' ) ) >= AuthComponent::user('License.product_limit') ) {
		// 	$this->Session->setFlash(__('Você adicionou todos os produtos'), 'default', array('class' => 'callout callout-danger'));
		// 	return $this->redirect(array('action' => 'index'));
		// }
	}

	public function edit( $type = null, $product_code = null, $message_code ) {
		$message = $this->Message->find( 'first', array( 'conditions' => array( 'Message.message_code' => $message_code ) ) );
		$id = $message['Message']['id'];
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Essa Mensagem não existe!'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('Salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'index', $this->request->data[ 'Message' ][ 'type' ], $this->request->data[ 'Message' ][ 'product_code' ]));
			} else {
				$this->Session->setFlash(__('Não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->request->data = $this->Message->find('first', $options);

			$this->set( 'type', $type );
			$this->set( 'product_code', $product_code );
			$this->set( 'product', $this->productDetails( $product_code ) );
		}
	}

	public function delete( $type = null, $product_code = null, $message_code = null ) {
		$message = $this->Message->find( 'first', array( 'conditions' => array( 'Message.message_code' => $message_code ) ) );
		$this->Message->id = $message[ 'Message' ][ 'id' ];
		if (!$this->Message->exists()) {
			throw new NotFoundException(__('Essa Mensagem não existe!'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Message->delete()) {
			$this->Session->setFlash(__('Mensagem excluída'), 'default', array('class' => 'callout callout-success'));
			return $this->redirect(array('action' => 'index', $type, $product_code));
		}
		$this->Session->setFlash(__('Mensagem não foi excluída'), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index', $this->request->data[ 'Message' ][ 'type' ], $this->request->data[ 'Message' ][ 'product_code' ]));
	}

	private function productDetails( $product_code ) {
		$this->loadModel( 'Product' );
		return $this->Product->find( 'first', array( 'conditions' => array( 'Product.product_code' => $product_code ) ) );
	}

	private function getMessageIds( $messages ) {
		$ids = array();
		foreach ( $messages as $m ) {
			$ids[] = $m[ 'Message' ][ 'id' ];
		}

		return $ids ;
	}
}
