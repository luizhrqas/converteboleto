<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AuthController extends AppController {

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('index');
	}

	public function index(){
		$this->autoRender = false;
      $this->layout = false;

		$this->loadModel('License');

      $count = 0;

      $data['License']['domain']  = $_REQUEST['domain'];
		$data['License']['email'] 	 = $_REQUEST['email'];
		$data['License']['key'] 	 = $_REQUEST['key'];
		$data['License']['secret']  = $_REQUEST['secret'];

      if ( !empty( $data['License']['domain'] ) && !empty( $data['License']['email'] ) && !empty( $data['License']['key'] ) && !empty( $data['License']['secret'] )) {

   		$options = array(
   			'conditions' => array(
   				'User.username' 	=> $data['License']['email'], 
               'License.domain'  => $data['License']['domain'],
   				'License.key' 		=> $data['License']['key'],
   				'License.secret' 	=> $data['License']['secret']
   			)
   		);

   		$count = $this->License->find('count', $options);
      }

		if ($count > 0) {
         return true; 
      } else {
         throw new ForbiddenException();
      }
	}
}
